//Importar la clase express

import express from 'express';


//Crear un objeto express
const app=express();
const puerto=3001;

// Crear una ruta

app.get("/bienvenida", (req, res)=>{

    res.send("Bienvenido al mundo de backend Node.");
});

app.get("/sumar", (peticion, respuesta)=>{

     let resultado=Number(peticion.query.a)+Number(peticion.query.b);

    respuesta.send(resultado.toString());
});

//Ruta Sumar

app.get("/sumar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)+Number(peticion.query.b);
    
    respuesta.send(resultado.toString());

});

//Ruta restar

app.get("/restar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)-Number(peticion.query.b);
    
    respuesta.send(resultado.toString());

});

//Ruta Multiplicar

app.get("/multiplicar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)*Number(peticion.query.b);
    
    respuesta.send(resultado.toString());

});

//Ruta Modular

app.get("/modular", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)%Number(peticion.query.b);
    
    respuesta.send(resultado.toString());

});

//Ruta Dividir

app.get("/dividir", (peticion, respuesta)=>{
            
    let a
    let b
    let resultado=Number(peticion.query.a)/Number(peticion.query.b);
    
    if(b===0){
        let respuesta;
        respuesta.send("No se puede dividir el número en cero")
        
    }else {
        respuesta.send(resultado.toString());
    }

});

//Inicializar el servidor node

app.listen(puerto, ()=>{console.log("Esta funcionando el servidor.")});
